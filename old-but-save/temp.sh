#!/bin/bash
set -euo pipefail
export AWS_PAGER=""
#PUBLIC_IP=$(cat /tmp/iplist.txt | cut -f2 -d$'\t' | tr -d '[:space:]') 
PUBLIC_IP=$(doctl compute droplet list | grep apache2 | awk '{print $3}' )
PUBLIC_IP='206.189.186.19'
HOSTED_ZONE_ID=$(aws route53 list-hosted-zones-by-name --output json |  jq '.HostedZones[0].Id' | cut -d / -f 3 | tr -d '"')
A_RECORD=mysql.auto-deploy.net
cat << EOF > create-a-record.json
{
	"Comment":"Creates or update an A record",
	"Changes":[{
		"Action":"UPSERT",
		"ResourceRecordSet": {
			"Name": "${A_RECORD}",
			"Type":"A",
			"TTL":30, 
			"ResourceRecords":[{
				"Value": "${PUBLIC_IP}"
			}]
		}
	}]
}
EOF
echo "HOSTED_ZONE_ID...: $HOSTED_ZONE_ID"
echo "A_RECORD.........: $A_RECORD"
echo "PUBLIC_IP........: $PUBLIC_IP"
aws --output=json route53 change-resource-record-sets --hosted-zone-id ${HOSTED_ZONE_ID} --change-batch file://${PWD}/create-a-record.json 

