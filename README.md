#### Public Facing Database Sever ####
- This is a public facing database that is running a MYSQL container 
- There should only be rsa key ssh access for users and MYSQL connections

#### Discussed and agreed ##### 
1) update Cloudflare DNS
2) add and setup for user root / "ej" with sudo root "nopassword" attribute
3) add user with private key; so that is a user on the VM that can interact with "gitlab - advocatediablo repo" 

#### Checklist Container ##### 
1) Does mysql command work? 
2) Is the Dockerfile needed further?
3) How to get a user added with private key so that it can run "git" commmands and backup the server?
4) Add a regular interactive user & public key?
5) Where does the IP variable go on outputs.tf
6) Need to added and test ssh keys for mysql
7) Status of nginx ?
8) Have a browsed folder of files?

#### Checklist Database ##### 
1) Is mysql working on the VM
2) How do we add users?  Read user and read write user?
3) Move scripts over from mysql repo

#### Cloudflare Instructions ##### 
https://www.codeproject.com/Articles/5259968/Terraform-Setting-Up-Domain-DNS-with-Cloudflare


#### About the site caretaker #### 
EJ Best
https://www.linkedin.com/in/ejbest/