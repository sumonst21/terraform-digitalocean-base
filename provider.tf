# Terraform Block
terraform {
    required_version = "~> 1.0.1"
    required_providers {
    digitalocean = {
      #source = "terraform-providers/digitalocean"
      source = "digitalocean/digitalocean"
    }
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
}

provider "cloudflare" {
  api_token = var.CLOUDFLARE_API_TOKEN
}

# Provider Block
provider "digitalocean" {
  token = var.DIGITIAL_OCEAN
}
