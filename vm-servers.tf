# Root SSH Key
resource "digitalocean_ssh_key" "root" {
  name       = "Root Public Key"
  public_key = "${var.DBUSER_PUBLIC_KEY}"
}

resource "digitalocean_droplet" "bastiondb" {
  image = "centos-8-x64"
  name = "bastiondb"
  region = "nyc1"
  size = "s-1vcpu-2gb"
  #private_networking = true
  ssh_keys = [
    digitalocean_ssh_key.root.fingerprint
  ]
  user_data = data.template_file.cloud-init-yaml.rendered

  provisioner "remote-exec" {
    connection {
      type     = "ssh"
      user     = "root"
      private_key = "${var.DBUSER_PRIVATE_KEY}"
      timeout  = "2m"
      host = "${digitalocean_droplet.bastiondb.ipv4_address}"
    }
    inline = [
      "echo 'dbuser-backup ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers; adduser dbuser-backup; mkdir -p /home/dbuser-backup/.ssh; chmod 700 /home/dbuser-backup/.ssh; echo '${var.DBUSER_PUBLIC_KEY}' >> /home/dbuser-backup/.ssh/authorized_keys; chmod 600 /home/dbuser-backup/.ssh/authorized_keys; chown -R dbuser-backup:dbuser-backup /home/dbuser-backup",
      "echo 'ej ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers; adduser ej; mkdir -p /home/ej/.ssh; chmod 700 /home/ej/.ssh; echo '${var.DBUSER_PUBLIC_KEY}' >> /home/ej/.ssh/authorized_keys; chmod 600 /home/ej/.ssh/authorized_keys; chown -R ej:ej /home/ej"
    ]
  }
}

data "cloudflare_zone" "advocatediablo_autodeploy" {
  name = "auto-deploy.net"
}
resource "cloudflare_record" "advocatediablo_autodeploy" {
  zone_id   = data.cloudflare_zone.advocatediablo_autodeploy.id
  name    = "mysql"
  value   = "${digitalocean_droplet.bastiondb.ipv4_address}"
  type    = "A"
  proxied = false
}

data "template_file" "cloud-init-yaml" {
  template = file("${path.module}/cloud-init.yaml")
}
